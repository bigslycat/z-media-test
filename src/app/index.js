/**
 * Я не стал ничего комментировать в этом файле, т.к. здесь находится код, не
 * относящийся к реализации проекта (в основном, навешиваются обработчики
 * событий). Его тоже нужно смотреть, но здесь всё понятно. Далее по порядку
 * нужно смотреть bootstrap.js.
 */

import $ from 'jquery';

import { controller } from './bootstrap';
import config from './config';

const { dragTolerance } = config;

const $doc = $(document);

$(() => {
  const $header = $('.Header');
  const $currentScore = $header.find('.Header-ScoreValue');

  $currentScore.on('update', (currentEvent, score) => {
    $(currentEvent.target).text(score);
  });

  const $playField = $('.PlayField');
  const $modal = $playField.find('.PlayField-Modal');

  function getBoxClass(x, y) {
    return `PlayField-Box--place-x${x}y${y}`;
  }

  function getBox(x, y) {
    return $playField.find(`.${getBoxClass(x, y)}`);
  }

  const placesCache = {
    singleton(x, y) {
      const elementName = `$x${x}y${y}`;

      if (!this[elementName]) {
        this[elementName] = $playField.find(
          `.PlayField-Place--row-${y}` +
          `.PlayField-Place--column-${x}`
        );
      }

      return this[elementName];
    },
  };

  $playField.on(
    'updatePosition',
    '.PlayField-Box',
    (currentEvent, x, y) => {
      $(currentEvent.target).offset(placesCache.singleton(x, y).offset());
    }
  );

  controller
    .on('boxBorn', ({ x, y }) => {
      const $box = $('<div/>', { 'class': [
        'PlayField-Box',
        'PlayField-Box--hidden',
        getBoxClass(x, y),
      ].join(' ') });

      $box.appendTo($playField);

      $box.trigger('updatePosition', [x, y]);
      $box.removeClass('PlayField-Box--hidden');
    })

    .on('boxGo', ({ prevX, prevY, x, y }) => {
      const $box = getBox(prevX, prevY);

      $box
        .addClass('PlayField-Box--inMove')
        .removeClass('PlayField-Box--inMove')
        .trigger('updatePosition', [x, y])
        .removeClass(getBoxClass(prevX, prevY))
        .addClass(getBoxClass(x, y));
    })

    .on('boxScoreUpdate', ({
      x, y,
      gameScore,
      prevBoxScore,
      currentBoxScore,
    }) => {
      $currentScore.trigger('update', gameScore);

      const $box = getBox(x, y);

      $box
        .removeClass(`PlayField-Box--number-${prevBoxScore}`)
        .addClass(`PlayField-Box--number-${currentBoxScore}`)
        .text(currentBoxScore);
    })

    .on('boxDead', ({ x, y }) => {
      getBox(x, y).addClass('PlayField-Box--hidden').remove();
    })

    .on('gameOver', () => $modal.removeClass('PlayField-Modal--off'));

  const $ok = $modal.find('.Button--ok');

  $ok.on('click', () => $modal.addClass('PlayField-Modal--off'));

  $('.Button--repeat').on('click', () => {
    $ok.trigger('click');
    controller.newGame();
  });

  $playField.on('mousedown', ({ originalEvent: {
    pageX: startX,
    pageY: startY,
  } }) => {
    $doc.on('mouseup', function stopDirectionCapture({
      originalEvent: {
        pageX: stopX,
        pageY: stopY,
      },
    }) {
      const vertical = stopY > startY ? 'down' : 'up';
      const horizontal = stopX > startX ? 'right' : 'left';

      const differenceX = Math.abs(stopX - startX);
      const differenceY = Math.abs(stopY - startY);

      if (
        differenceX + differenceY > dragTolerance
      ) controller.go(differenceY > differenceX ? vertical : horizontal);

      $doc.off('mouseup', stopDirectionCapture);
    });
  });

  /**
   * С клавиатуры, всё-таки, удобнее.
   */
  $doc.on('keydown', currentEvent => {
    let direction;

    switch (currentEvent.which) {
      case 40: {
        direction = 'down';
        break;
      }

      case 39: {
        direction = 'right';
        break;
      }

      case 38: {
        direction = 'up';
        break;
      }

      case 37: {
        direction = 'left';
        break;
      }

      default: {
        return;
      }
    }

    controller.go(direction);
  });

  controller.newGame();
});
