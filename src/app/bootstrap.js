import config from './config';
import Place from './entities/Place';
import gameScope from './gameScope';

/**
 * Скоуп всех ячеек
 */
export const places = {
  /**
   * Общий скоуп
   */
  commonScope: [],

  /**
   * Массив строк
   */
  rows: [],

  /**
   * Массив колонок
   */
  columns: [],

  /**
   * Получить все свободные ячейки
   * @returns {Array}
   */
  get free() {
    return this.commonScope.filter(place => place.isFree);
  },

  /**
   * Получить все занятые ячейки
   * @returns {Array}
   */
  get busy() {
    return this.commonScope.filter(place => place.isBusy);
  },

  /**
   * Получить одна случайную свободная ячейка
   * @returns {Place}
   */
  get randomFree() {
    const free = this.free;
    const index = Math.floor(Math.random() * free.length);
    return free[index];
  },

  /**
   * Получить все ячейки, занятые блоками, которые могут сделать ход
   * @returns {Place}
   */
  get notBlocked() {
    return this.commonScope.filter(
      place => place.box && !place.box.isBlocked
    );
  },

  /**
   * Очистить все занятые ячейки
   */
  clearAll() {
    this.busy.forEach(place => place.clear());
    return this;
  },
};

/**
 * Получаем рамер игрового поля из конфига (кол-во ячеек)
 */
const { fieldSize } = config;

/**
 * Заполняем игровое поля ячейками
 */
for (let rowIndex = 0; rowIndex < fieldSize; rowIndex++) {
  /**
   * Создаём новый скоуп строки
   */
  const row = [];

  /**
   * Добавляем строку в скоуп строк
   */
  places.rows.push(row);

  for (let columnIndex = 0; columnIndex < fieldSize; columnIndex++) {
    /**
     * Добавляем колонку в скоуп колонок, если её ещё там нет, и выбираем её
     */
    if (!places.columns[columnIndex]) places.columns.push([]);
    const column = places.columns[columnIndex];

    /**
     * Создаём экземпляр ячейки
     */
    const place = new Place({ //   Отдаём ему:
      row,                    // - Ссылку на скоуп строк
      column,                 // - Ссылку на скоуп колонок
      rowIndex,               // - Индекс строки
      columnIndex,            // - Индекс колонки
    });

    /**
     * Пишем ячейку в строку, в колонку и в общий скоуп
     */
    row.push(place);
    column.push(place);
    places.commonScope.push(place);
  }
}

export const controller = {
  /**
   * Старт/рестарт игры
   */
  newGame() {
    places.clearAll();
    gameScope.reset();
    places.randomFree.newBox();
  },

  /**
   * Повесить обработчик на какое-либо событие в игре
   * @arg {string} eventName Имя события
   * @arg {function} callback Колбэк
   * @returns {object} this
   */
  on(eventName, callback) {
    if (eventName.slice(0, 3) === 'box') {
      const boxEventName = eventName.slice(3);

      if (boxEventName) gameScope[`on${boxEventName}`] = callback;
    } else if ([
      'gameOver',
    ].includes(eventName)) {
      this.onGameOver = callback;
    }

    return this;
  },

  /**
   * Проверить состояние «Game over», наличие соответсвующего обработчика и
   * вызвать его
   */
  tryGameOver() {
    if (
      !places.free.length &&
      !places.notBlocked.length &&
      this.onGameOver
    ) this.onGameOver(gameScope.scores);
  },

  /**
   * Вызывается, когда ход завершён, если он привёл к изменениям
   */
  onStepComplete() {
    places.randomFree.newBox();

    /**
     * Разрешаем всем есть соседей
     */
    places.busy.forEach(place => {
      place.box.eatingOrder = true;
    });

    this.tryGameOver();
  },

  /**
   * Сделать ход
   * @arg {string} direction Направление
   * @arg {boolean} first Является ли итерация первой в рамках одного хода
   */
  go(direction, first = true) {
    /**
     * В зависимости от того, какое у нас есть направление, горизонтальное или
     * вертикальное, мы будем работать либо со строками, либо с колонками. Чтобы
     * не плодить копипасту, мы должны абстрактизировать эти вещи.
     */
    let scope;

    /**
     * Если направление вертикальное, работаем с колонками
     */
    if (['up', 'down'].includes(direction)) scope = places.columns;

    /**
     * Если горизонтальное — со строками
     */
    else if (['left', 'right'].includes(direction)) scope = places.rows;

    /**
     * Если какое-то непонятное, то мы ничего не можем с этим поделать
     */
    else return;

    /**
     * Если направление «обратное», т.е. противоположное инкременту индекса, то
     * нам будет нужно инвертировать скоуп. Проверяем, так или это, заранее:
     */
    const needReverse = !['up', 'left'].includes(direction);

    /**
     * Флаг необходимости следующей итерации
     */
    let nextTry = false;

    /**
     * Обходим колонки или строки
     */
    scope.forEach(subScope => {
      let currentPlaces;

      if (needReverse) {
        /**
         * Т.к. Array.prototype.reverse() инвертирует исходный массив, нам
         * нужно его скопировать. Копируем:
         */
        currentPlaces = Array.from(subScope);

        /**
         * и работаем с копией
         */
        currentPlaces.reverse();
      } else {
        currentPlaces = subScope;
      }

      currentPlaces.forEach(place => {
        if (
          place.isBusy &&         // Если ячейка занята,
          place.box.go(direction) // и у нас получилось сделать ход,
        ) nextTry = true;         // значит нужно будет сделать ещё одну попытку
      });
    });

    if (nextTry) {
      /**
      * Если нужна следующая попытка, делаем её
      */
      this.go(direction, false);
    } else if (!first) {
      /**
       * Если нет, и это начало хода, то значит никаких изменений не было.
       * Поэтому this.onStepComplete() вызывается только во 2 итерации или
       * более.
       */
      this.onStepComplete();
    }
  },
};
