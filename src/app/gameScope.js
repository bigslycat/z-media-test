/**
 * Игровой скоуп
 */
export default {
  /**
   * Текущий счёт
   */
  score: 0,

  /**
   * Лучший счёт
   */
  best: 0,

  /**
   * Обработчики событий
   */
  handlers: {},

  /**
   * Сбросить счёт
   */
  reset() {
    this.score = 0;
    this.best = 0;
  },

  /**
   * Увеличить счёт на указанное значение
   */
  upScore(difference) {
    this.score += difference;
    if (this.score > this.best) this.best = this.score;
  },

  get current() {
    return {
      score: this.score,
      best: this.best,
    };
  },
};
