import Box from './Box';

/**
 * Класс, реализующий функциональность ячеек
 */
export default class Place {
  constructor(options) {
    /**
     * Пишем в экземпляр всё, что нам отправили
     */
    Object.assign(this, options);
  }

  /**
   * Создать новый блок в ячейке
   */
  newBox() {
    this.box = new Box(this);
    return this;
  }

  /**
   * Очистить ячейку
   */
  clear() {
    if (this.isBusy) this.box.del();
    return this;
  }

  /**
   * Получить следующую по направлению ячейку
   */
  next(direction) {
    return this[direction];
  }

  /**
   * Ячейка слева
   */
  get left() {
    return this.row[this.columnIndex - 1] || null;
  }

  /**
   * Ячейка справа
   */
  get right() {
    return this.row[this.columnIndex + 1] || null;
  }

  /**
   * Ячейка сверху
   */
  get up() {
    return this.column[this.rowIndex - 1] || null;
  }

  /**
   * Ячейка снизу
   */
  get down() {
    return this.column[this.rowIndex + 1] || null;
  }

  /**
   * Присутствует ли в ячейке блок
   */
  get isBusy() {
    return Boolean(this.box);
  }

  /**
   * Отсутсвует ли в ячейке блок
   */
  get isFree() {
    return !this.isBusy;
  }
}
