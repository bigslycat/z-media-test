import gameScope from '../gameScope';

/**
 * Класс, реализующий функциональность блока
 */
export default class Box {
  constructor(nextPlace) {
    /**
     * Запоминаем, где мы находимся
     */
    this.place = nextPlace;

    /**
     * Разрешено взаимодействие в другими ячейками
     */
    this.eatingOrder = true;

    /**
     * Публикуем состояние блока
     */
    this.publish('born');

    this.update(Math.random() < 0.1 ? 4 : 2);
  }

  /**
   * Вернёт true, если блок не может быть изменён в текущем состоянии
   */
  get isBlocked() {
    const directions = ['up', 'right', 'down', 'left'];

    for (let index = 0; index < directions.length; index++) {
      if (this.canIGo(directions[index])) return false;
    }

    return true;
  }

  /**
   * Может ли блок сделать ход в указанном направлении. Если да, вернёт
   * соседнюю целевую ячейку
   */
  canIGo(direction) {
    const nextPlace = this.place.next(direction);

    if (
      !nextPlace || (
        nextPlace.box && !this.canIEat(nextPlace.box)
      )
    ) return false;

    return nextPlace;
  }

  /**
   * Обновить значение блока
   */
  update(newValue, noPublish = false) {
    this.prevScore = this.score || 0;
    this.score = newValue;

    if (!noPublish) this.publish('scoreUpdate');

    return this;
  }

  /**
   * Удвоить значение блока
   */
  up() {
    this.update(this.score * 2, true);
    gameScope.upScore(this.score);

    this.publish('scoreUpdate');

    return this;
  }

  /**
   * Может ли текущий блок съесть указанный блок
   */
  canIEat(oldBox) {
    return (
      oldBox.score === this.score &&
      this.eatingOrder &&
      oldBox.eatingOrder
    );
  }

  /**
   * Съесть указанный блок
   */
  eat(oldBox) {
    if (this.canIEat(oldBox)) {
      this.up();
      oldBox.del();

      this.eatingOrder = false;
    }

    return this;
  }

  /**
   * Сделать ход в указанном направлении
   */
  go(direction) {
    /**
     * Можем ли мы идти в указанном направлении
     */
    const nextPlace = this.canIGo(direction);

    /**
     * Если не можем, то и не будем
     */
    if (!nextPlace) return false;

    /**
     * Съесть блок в целевой ячейке, если он там есть
     */
    if (nextPlace.isBusy) this.eat(nextPlace.box);

    /**
     * Удалить себя из текущей ячейки и записать в целевую
     */
    nextPlace.box = this.del(true);

    this.prevPlace = this.place;

    /**
     * Обновить ссылку на ячейку
     */
    this.place = nextPlace;

    /**
     * Публикуем состояние блока и дополнительно сообщаем обработчику его
     * предыдущие координаты.
     */
    this.publish('go');

    /**
     * Пробуем идти дальше
     */
    this.go(direction);

    /**
     * Снаружи нужно знать, были ли сделаны изменения
     */
    return true;
  }

  /**
   * Публикация состояния блока
   * @arg {string} subject Имя события (born|go|scoreUpdate|dead)
   */
  publish(subject) {
    /**
     * Имя события превращается в имя метода. Например, имя события "born"
     * превратится в имя метода "onBorn". Соответственно, далее будет вызван
     * метод gameScope.onBorn(), которым будет являться колбэк, установленный
     * снаружи модуля.
     */
    const methodName = `on${
      subject.charAt(0).toUpperCase() +
      subject.slice(1)
    }`;

    /**
     * Вызываем колбэк и передаём ему текущее (или последнее) состояние блока.
     */
    if (gameScope[methodName]) {
      gameScope[methodName]({
        prevX: this.prevPlace ? this.prevPlace.columnIndex : null,
        prevY: this.prevPlace ? this.prevPlace.rowIndex : null,
        prevBoxScore: this.prevScore,
        x: this.place.columnIndex,
        y: this.place.rowIndex,
        currentBoxScore: this.score,
        gameScore: gameScope.current.score,
        gameScoreBest: gameScope.current.best,
      });
    }
  }

  /**
   * Самоудаление
   * @arg {boolean} noPublish Не публиковать событие
   *                          Используется, когда нужно удалить блок из текущей
   *                          ячейки и записать в новую
   * @returns {Box} this
   */
  del(noPublish = false) {
    if (!noPublish) {
      /**
       * Публикуем состояние блока
       */
      this.publish('dead');
    }

    delete this.place.box;

    return this;
  }
}
