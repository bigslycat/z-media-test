'use strict';

const path = require('path');

const gulp = require('gulp');
const less = require('gulp-less');
const concat = require('gulp-concat');
const LessAutoprefix = require('less-plugin-autoprefix');
const pug = require('gulp-pug');
const webpackStream = require('webpack-stream');

const entries = require('object.entries');

if (!Object.entries) entries.shim();

const srcDir = path.join(__dirname, 'src');
const buildDir = path.join(__dirname, 'build');

const stylesDirName = 'styles';

const stylesDirSrc = path.join(srcDir, stylesDirName);
const stylesDirDest = path.join(buildDir, stylesDirName);

const cssFilename = 'main.css';

const normalizeCssPath = path.join(
  __dirname, 'node_modules', 'normalize.css', 'normalize.css'
);

const wtfHack = path.join(stylesDirSrc, 'wtf-hacks.less');
const mainLessPath = path.join(stylesDirSrc, 'main.less');

const lessOptions = { plugins: [
  new LessAutoprefix([
    'last 5 Chrome versions',
    'last 5 Firefox versions',
    'iOS >= 8',
    'ie >= 9',
  ]),
] };

const polyfillFeatures = [
  'Promise',
  'Array.from',
  'Array.prototype.includes',
  'Array.prototype.filter',
  'Array.prototype.forEach',
];

const polyfillUrlBase = 'https://cdn.polyfill.io/v2/polyfill.js';
const polyfillUrl = `${polyfillUrlBase}?features=${polyfillFeatures.join(',')}`;

const appJsFilename = 'index.js';
const appJsSrcDir = path.join(srcDir, 'app');
const appJsSrc = path.join(appJsSrcDir, 'index.js');
const appJsDestDir = path.join(buildDir, 'app');
const appJsDest = path.join(appJsDestDir, appJsFilename);

const webpackConfig = {
  output: {
    filename: appJsFilename,
    publicPath: `/${path.relative(buildDir, appJsDestDir)}/`,
  },
  module: { loaders: [{
    loader: 'babel',
    query: {
      presets: ['es2015', 'es2016'],
      env: { BABEL_DISABLE_CACHE: 1 },
    },
  }] },
  target: 'web',
  devtool: '#source-map',
};

const appConfig = require(path.join(appJsSrcDir, 'config'));

const pugOptions = {
  locals: {
    appConfig,
    polyfillUrl,
    appJs: `/${path.relative(buildDir, appJsDest)}`,
    appCss: `/${path.join(
      path.relative(buildDir, stylesDirDest), cssFilename
    )}`,
  },
  pretty: true,
};

gulp.task('styles', () =>
  gulp.src([normalizeCssPath, wtfHack, mainLessPath])
  .pipe(concat(cssFilename))
  .pipe(less(lessOptions))
  .pipe(gulp.dest(stylesDirDest))
);

gulp.task('templates', () =>
  gulp.src(path.join(srcDir, 'templates', 'index.pug'))
  .pipe(pug(pugOptions))
  .pipe(gulp.dest(buildDir))
);

gulp.task('app', () =>
  gulp.src(appJsSrc)
  .pipe(webpackStream(webpackConfig))
  .pipe(gulp.dest(appJsDestDir))
);

function onChange(event) {
  console.log(
    `File ${event.path} was ${event.type} running tasks...`
  );
}

Object.entries({
  styles: 'less',
  templates: 'pug',
  app: 'js',
}).forEach(([task, extension]) => gulp.watch(
  path.join(srcDir, '**', `*.${extension}`), [task]
).on('change', onChange));
